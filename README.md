<h1>PRACTICA 1 - MANIPULACIÓ DE REPOSITORI AMB GIT</h1>

<h2>Anem a fer algunes pràctiques amb el git</h2>



pràctica:
git clone https://gitlab.com/alextec/practica-git.git
Puja un fitxer amb tota aquesta la seqüència de comandes

<ol>
<li>Crea una branca anomenada practica_[el teu cognom]</li>
<li>Mou-te a la branca creada</li>
<li>Crea un fitxer [el teu nom].txt</li>
<li>A dins // CREAT [data hora]</li>
<li>Afegeix-lo a l’index</li>
<li>Fes un commit “Creat fitxer [el teu cognom].txt”</li>
<li>Fes push cap el repositori,</li>

<li>veuràs que al remote no existeix la branca que has creat en local, per això
cal crear-la per poder pujar els canvis:
git push --set-upstream origin practica_[cognom]</li>
<li>Comprova que el nou fitxer és al remote</li>
https://github.com/icastelletdam2/practiques_DAM2/tree/practica_alex/mino/erm
al/engine3d</li>
<li>Crea un fitxer comu.txt</li>
<li>Posa-li a dins // MODIFICAT [cognom]</li>
<li>Ara, afegeix al fitxer [el teu cognom].txt //MODIFICAT [data hora]</li>
<li>Fes commit</li>
<li>Crea i mou-te a una nova branca branca2_[cognom]</li>
<li>Ara, afegeix al fitxer [el teu cognom].txt //MODIFICAT A branca2 [data hora]</li>
<li>Fes commit</li>
<li>Ara, afegeix al fitxer [el teu cognom].txt //MODIFICAT **22222*** A branca2
[data hora]</li>
<li>Fes commit</li>
<li>afegeix al fitxer [el teu cognom].txt //MODIFCAT **33333*** A branca2 [data
hora]</li>
<li>Posa els canvis no commited a l’stash</li>
Agafa la branca master</li>
<li>Recupera l’stash</li>
<li>Ara, afegeix al fitxer [el teu cognom].txt //MODIFICAT A master [data hora]</li>
<li>Fes commit</li>
<li>Fes push</li>


<h2>SEGONA PART
Ara practicarem la capacitat de GIT per tornar a un estat anterior </h2>

<li> Modifica el fitxer[el teu cognom].txt afegint aquestes linies:

/*************************************/
AIXÒ ÉS UN ERROR
/*************************************/
AQIXò ES FEINA ÚTIL
/*************************************/

<li> Afegeix-lo a l'index i fes commit</li>
<li>Ara fes un git reset --soft HEAD~1</li>
<li>Fes un git status. Enganxa els resultat al fitxer i explica què ha passat.</li> 
<li>Fes commit d'aquests darrers canvis</li>
<li>Ara fes un reset --HARD al commit on hem començat aquesta segona part</li>
<li>Fes un git status. Enganxa els resultat al fitxer i explica què ha passat.</li> 
<li>Fes commit d'aquests darrers canvis</li>

